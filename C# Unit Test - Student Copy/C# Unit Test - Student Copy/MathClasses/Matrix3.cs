﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathClasses
{
    public class Matrix3
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9;

        public static Matrix3 Identity
        {
            get
            {
                return new Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1);
            }
        }

        public Vector3 GetRow(uint row)
        {
            switch (row)
            {
                case 0:
                    return new Vector3(m1, m4, m7);
                case 1:
                    return new Vector3(m2, m5, m8);
                case 2:
                    return new Vector3(m3, m6, m9);
            }
            return new Vector3();
        }

        public Vector3 GetCol(uint col)
        {
            switch (col)
            {
                case 0:
                    return new Vector3(m1, m2, m3);
                case 1:
                    return new Vector3(m4, m5, m6);
                case 2:
                    return new Vector3(m7, m8, m9);
            }
            return new Vector3();
        }

        public Matrix3()
        {
            m1 = 1;
            m2 = 0;
            m3 = 0;

            m4 = 0;
            m5 = 1;
            m6 = 0;

            m7 = 0;
            m8 = 0;
            m9 = 1;
        }

        public Matrix3(float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9)
        {
            Set(m1, m2, m3, m4, m5, m6, m7, m8, m9);
        }

        public void Set(Matrix3 m)
        {
            m1 = m.m1;
            m2 = m.m2;
            m3 = m.m3;

            m4 = m.m4;
            m5 = m.m5;
            m6 = m.m6;

            m7 = m.m7;
            m8 = m.m8;
            m9 = m.m9;
        }


        public void Set(Vector3 X, Vector3 Y, Vector3 Z)
        {
            m1 = X.x;
            m2 = X.y;
            m3 = X.z;

            m4 = Y.x;
            m5 = Y.y;
            m6 = Y.z;

            m7 = Z.x;
            m8 = Z.y;
            m9 = Z.z;
        }

        public void Set(float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9)
        {
            this.m1 = m1;
            this.m2 = m2;
            this.m3 = m3;

            this.m4 = m4;
            this.m5 = m5;
            this.m6 = m6;

            this.m7 = m7;
            this.m8 = m8;
            this.m9 = m9;
        }

        public void SetRotateX(double radians)
        {
            Set(1, 0, 0,
                0, (float)Math.Cos(radians), (float)Math.Sin(radians),
                0, -(float)Math.Sin(radians), (float)Math.Cos(radians)); 
        }

        public void SetRotateY(double radians)
        {
            Set((float)Math.Cos(radians), 0, -(float)Math.Sin(radians),
                0, 1, 0,
                (float)Math.Sin(radians), 0, (float)Math.Cos(radians));
        }

        public void SetRotateZ(double radians)
        {
            Set((float)Math.Cos(radians), (float)Math.Sin(radians), 0,
                -(float)Math.Sin(radians), (float)Math.Cos(radians), 0,
                0, 0, 1);
        }
        
        public static Matrix3 operator*(Matrix3 lhs, Matrix3 rhs)
        {
            return new Matrix3(lhs.GetRow(0).Dot(rhs.GetCol(0)),
                                lhs.GetRow(1).Dot(rhs.GetCol(0)),
                                lhs.GetRow(2).Dot(rhs.GetCol(0)),

                                lhs.GetRow(0).Dot(rhs.GetCol(1)),
                                lhs.GetRow(1).Dot(rhs.GetCol(1)),
                                lhs.GetRow(2).Dot(rhs.GetCol(1)),

                                lhs.GetRow(0).Dot(rhs.GetCol(2)),
                                lhs.GetRow(1).Dot(rhs.GetCol(2)),
                                lhs.GetRow(2).Dot(rhs.GetCol(2)));
        }
        
        public static Vector3 operator *(Matrix3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.GetRow(0).Dot(rhs),
                                lhs.GetRow(1).Dot(rhs),
                                lhs.GetRow(2).Dot(rhs));
        }

        /*
        public static Vector3 operator *(Matrix3 lhs, Vector3 rhs)
        {
            return new Vector3(
                lhs.m1 * rhs.x + lhs.m4 * rhs.y + lhs.m7 * rhs.z,
                lhs.m2 * rhs.x + lhs.m5 * rhs.y + lhs.m8 * rhs.z,
                lhs.m3 * rhs.x + lhs.m6 * rhs.y + lhs.m9 * rhs.z);
        }
        */
    }
}