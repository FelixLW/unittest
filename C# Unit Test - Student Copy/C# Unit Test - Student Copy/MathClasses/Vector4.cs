﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathClasses
{
    public class Vector4
    {
        public float x, y, z, w;

        public Vector4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 0;
        }
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static Vector4 operator +(Vector4 lhs, Vector4 rhs)
        {
            return new Vector4(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
        }

        public static Vector4 operator -(Vector4 lhs, Vector4 rhs)
        {
            return new Vector4(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w);
        }

        public static Vector4 operator *(Vector4 lhs, float scale)
        {
            return new Vector4(lhs.x * scale, lhs.y * scale, lhs.z * scale, lhs.w * scale);
        }
        
        public static Vector4 operator *(float scale, Vector4 rhs)
        {
            return new Vector4(scale * rhs.x, scale * rhs.y, scale * rhs.z, scale * rhs.w);
        }

        public float Dot(Vector4 other)
        {
            return (x * other.x) + (y * other.y) + (z * other.z) + (w * other.w);
        }

        public Vector4 Cross(Vector4 other)
        {
            return new Vector4(y * other.z - z * other.y,
                                z * other.x - x * other.z,
                                x * other.y - y * other.x,
                                0);
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt(this.Dot(this));
        }

        public void Normalize()
        {
            float length = Magnitude();
            this.x /= length;
            this.y /= length;
            this.z /= length;
            this.w /= length;
        }
    }
}
