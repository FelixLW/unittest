﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathClasses
{
    public class Vector3
    {
        public float x, y, z;

        public Vector3()
        {
            x = 0f;
            y = 0f;
            z = 0f;
        }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 operator +(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
        }

        public static Vector3 operator -(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
        }

        public static Vector3 operator *(Vector3 lhs, float scale)
        {
            return new Vector3(lhs.x * scale, lhs.y * scale, lhs.z * scale);
        }

        public static Vector3 operator /(Vector3 lhs, float scale)
        {
            return new Vector3(lhs.x / scale, lhs.y / scale, lhs.z / scale);
        }

        public static Vector3 operator *(float scale, Vector3 rhs)
        {
            return new Vector3(scale * rhs.x, scale * rhs.y, scale * rhs.z);
        }

        public float Dot(Vector3 other)
        {
            return (x * other.x) + (y * other.y) + (z * other.z);
        }

        public Vector3 Cross(Vector3 other)
        {
            return new Vector3(y * other.z - z * other.y,
                                z * other.x - x * other.z,
                                x * other.y - y * other.x);
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt(this.Dot(this));
        }

        public void Normalize()
        {
            float length = Magnitude();
            this.x /= length;
            this.y /= length;
            this.z /= length;
        }
    }
}
