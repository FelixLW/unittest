﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Project2D
{
    class SceneObject
    {
        protected SceneObject parent = null;
        protected List<SceneObject> children = new List<SceneObject>();

        private List<SceneObject> addChildPending = new List<SceneObject>();
        private List<SceneObject> removeChildPending = new List<SceneObject>();

        protected Matrix3 localTransform = new Matrix3();
        protected Matrix3 globalTransform = new Matrix3();

        public SceneObject Parent
        {
            get { return parent; }
        }

        public SceneObject()
        {

        }

        ~SceneObject()
        {
            if (parent != null)
            {
                parent.RemoveChild(this);
            }
            foreach(SceneObject so in children)
            {
                so.parent = null;
            }
        }

        public int GetChildCount()
        {
            return children.Count();
        }

        public SceneObject GetChild(int index)
        {
            return children[index];
        }

        public void AddChild(SceneObject child)
        {
            //Make sure it doesnt have a parent already
            Debug.Assert(child.parent == null);

            //Make this as the parent
            child.parent = this;

            //Add new child to the collection
            addChildPending.Add(child);
        }

        public void RemoveChild(SceneObject child)
        {
            if(child.parent == this)
            {
                child.parent = null;

                removeChildPending.Add(child);
            }
        }

        public void RemoveChildren()
        {
            foreach(SceneObject child in children)
            {
                RemoveChild(child);
            }
        }

        public virtual void OnUpdate(float deltaTime)
        {

        }

        public virtual void OnDraw()
        {

        }

        public void Update(float deltaTime)
        {
            foreach(SceneObject child in addChildPending)
            {
                children.Add(child);
                child.UpdateTransform();
            }
            addChildPending.Clear();

            foreach(SceneObject child in removeChildPending)
            {
                children.Remove(child);
                child.UpdateTransform();
            }
            removeChildPending.Clear();

            //
            OnUpdate(deltaTime);

            foreach(SceneObject child in children)
            {
                child.Update(deltaTime);
            }
        }

        public void Draw()
        {
            OnDraw();

            foreach(SceneObject child in children)
            {
                child.Draw();
            }
        }

        public Matrix3 LocalTransform
        {
            get { return localTransform; }
        }

        public Matrix3 GlobalTransform
        {
            get { return globalTransform; }
        }

        public void UpdateTransform()
        {
            if (parent != null)
            {
                globalTransform = parent.globalTransform * localTransform;
            }
            else
            {
                globalTransform = localTransform;
            }

            foreach(SceneObject child in children)
            {
                child.UpdateTransform();
            }
        }

        public void CopyTransformToLocal(Matrix3 m)
        {
            localTransform.Set(m);
            UpdateTransform();
        }

        public void SetPosition(float x, float y) 
        { 
            localTransform.SetTranslation(x, y); 
            UpdateTransform(); 
        }

        public void SetRotate(float radians) 
        { 
            localTransform.SetRotateZ(radians);
            UpdateTransform();
        }

        public void SetScale(float width, float height) 
        {
            localTransform.SetScaled(width, height, 1); 
            UpdateTransform(); 
        }

        public void Translate(float x, float y) 
        {
            localTransform.Translate(x, y); 
            UpdateTransform();
        }
        public void TranslateLocal(float rightAmound, float forwadAmound)
        {
            localTransform.Translate(rightAmound, forwadAmound);
            UpdateTransform();
        }

        public void Rotate(float radians) 
        {
            localTransform.RotateZ(radians); 
            UpdateTransform();
        }

        public void Scale(float width, float height)
        {
            localTransform.Scale(width, height, 1); 
            UpdateTransform();
        }
        
    }
}
