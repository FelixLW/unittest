﻿using static Raylib.Raylib;

namespace Project2D
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            SetTargetFPS(60);
            InitWindow(1280, 720, "MFG Tank Game");

            game.Init();

            while (!WindowShouldClose())
            {
                game.Update();
                game.Draw();
            }

            game.Shutdown();
            CloseWindow();
        }
    }
}
