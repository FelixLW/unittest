﻿using System;
using Raylib;
using static Raylib.Raylib;

namespace Project2D
{
    class Tank : SceneObject
    {
        SpriteObject tankSprite = new SpriteObject(@"..\sprites\tankBlack_outline.png");
        SpriteObject turretSprite = new SpriteObject(@"..\sprites\barrelRed_outline.png");
        //SpriteObject turretSprite = new SpriteObject(@"..\sprites\specialBarrel1_outline.png");
        SceneObject turretObject = new SceneObject();

        Game game;

        public float TurretHeight
        {
            get => turretSprite.Height;
        }

        public Tank(Game owner)
        {
            this.game = owner;
            SetPosition(GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f);

            tankSprite.SetRotate(-90 * (float)(Math.PI / 180.0f));
            tankSprite.SetPosition(-tankSprite.Width / 2.0f, tankSprite.Height / 2.0f);

            turretSprite.SetRotate(-90 * (float)(Math.PI / 180.0f));
            turretSprite.SetPosition(0, turretSprite.Width / 2.0f);
         
            turretObject.AddChild(turretSprite);
            AddChild(tankSprite);
            AddChild(turretObject);           
        }

        public override void OnUpdate(float deltaTime)
        {
            //tank controls
            {
                //rotate tank
                if (IsKeyDown(KeyboardKey.KEY_A)) //left
                {
                    Rotate(-deltaTime);
                }

                if (IsKeyDown(KeyboardKey.KEY_D)) //right
                {
                    Rotate(deltaTime);
                }

                //move tank
                if (IsKeyDown(KeyboardKey.KEY_W)) //fowards
                {
                    Vector3 facing = new Vector3(LocalTransform.m1, LocalTransform.m2, 1) * deltaTime * 100;
                    Translate(facing.x, facing.y);
                }

                if (IsKeyDown(KeyboardKey.KEY_S)) //backwards
                {
                    Vector3 facing = new Vector3(LocalTransform.m1, LocalTransform.m2, 1) * deltaTime * -100;
                    Translate(facing.x, facing.y);
                }
            }

            //turret controls
            {
                //rotate turret
                if (IsKeyDown(KeyboardKey.KEY_Q)) //left
                {
                    turretObject.Rotate(-deltaTime);
                }

                if (IsKeyDown(KeyboardKey.KEY_E)) //right
                {
                    turretObject.Rotate(deltaTime);
                }
            }

            //bullet controls
            {
                if (IsKeyPressed(KeyboardKey.KEY_SPACE)) //shoot
                {
                    Bullet b = new Bullet(game, this);

                    b.CopyTransformToLocal(turretObject.GlobalTransform);

                    

                    game.RootObj.AddChild(b);
                }
            }
        }
    }
}
