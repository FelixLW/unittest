﻿using System;

namespace Project2D
{
    class Vector3
    {
        public float x, y, z;

        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3(Vector3 v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
        }

        public static Vector3 operator +(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
        }

        public static Vector3 operator -(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
        }

        public static Vector3 operator *(Vector3 lhs, float scale)
        {
            return new Vector3(lhs.x * scale, lhs.y * scale, lhs.z * scale);
        }

        public static Vector3 operator /(Vector3 lhs, float scale)
        {
            return new Vector3(lhs.x / scale, lhs.y / scale, lhs.z / scale);
        }

        public static Vector3 operator *(float scale, Vector3 rhs)
        {
            return new Vector3(scale * rhs.x, scale * rhs.y, scale * rhs.z);
        }

        public float Dot(Vector3 other)
        {
            return (x * other.x) + (y * other.y) + (z * other.z);
        }

        public Vector3 Cross(Vector3 other)
        {
            return new Vector3(y * other.z - z * other.y,
                                z * other.x - x * other.z,
                                x * other.y - y * other.x);
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt(Dot(this));
        }

        public void Normalize()
        {
            float length = Magnitude();
            this.x /= length;
            this.y /= length;
            this.z /= length;
        }

        public Vector3 GetNormalised()
        {
            return (this / Magnitude());
        }

        public void Min()
        {

        }

        public void Max()
        {

        }

        public void Clamp()
        {

        }
    }
}
