﻿using System;
using static Raylib.Raylib;

namespace Project2D
{
    class Bullet : SceneObject
    {
        SpriteObject bullet = new SpriteObject();

        float speed = 100;
        const float maxspeed = 2000;
        const float acceleration = 500;

        Game game;

        public Bullet(Game owner, Tank tank)
        {
            game = owner;

            // Load bullet sprite
            bullet.Load(@"..\sprites\bulletSilverSilver_outline.png");
            bullet.SetRotate(90 * (float)(Math.PI / 180.0f));

            // Offset to parent object
            bullet.SetPosition(tank.TurretHeight + 25, -10);

            // Add to rootObject
            AddChild(bullet);
        }

        public override void OnUpdate(float deltaTime)
        {
            // Accelerate until max speed
            if (speed < maxspeed)
            {
                speed += acceleration * deltaTime;

                if (speed > maxspeed)
                    speed = maxspeed;
            }

            // Move

            float translationDistance = speed * deltaTime;
            Vector3 facing = new Vector3(globalTransform.m1, globalTransform.m2, 1) * translationDistance;
            Translate(facing.x, facing.y);

            
            // Collision check on bullet for gameborders

            if ((globalTransform.x < 0) || (globalTransform.x > GetScreenWidth()) || (globalTransform.y < 0) || (globalTransform.y > GetScreenHeight()))
            {
                Console.WriteLine("Bullet is out of bounds");
                game.RootObj.RemoveChild(this);
            }
        }

    }
}
