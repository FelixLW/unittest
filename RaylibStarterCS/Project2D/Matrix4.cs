﻿using System;

namespace Project2D
{
    class Matrix4
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16;

        public Matrix4()
        {
            m1 = 1;
            m2 = 0;
            m3 = 0;
            m4 = 0;

            m5 = 0;
            m6 = 1;
            m7 = 0;
            m8 = 0;

            m9 = 0;
            m10 = 0;
            m11 = 1;
            m12 = 0;

            m13 = 0;
            m14 = 0;
            m15 = 0;
            m16 = 1;
        }

        public Matrix4(float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9, float m10, float m11, float m12, float m13, float m14, float m15, float m16)
        {
            Set(m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16);
        }

        public Matrix4(Matrix4 m)
        {
            m1 = m.m1;
            m2 = m.m2;
            m3 = m.m3;
            m4 = m.m4;

            m5 = m.m5;
            m6 = m.m6;
            m7 = m.m7;
            m8 = m.m8;

            m9 = m.m9;
            m10 = m.m10;
            m11 = m.m11;
            m11 = m.m12;

            m13 = m.m13;
            m14 = m.m14;
            m15 = m.m15;
            m16 = m.m16;

        }

        public static Matrix4 Identity
        {
            get 
            { 
                return new Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); 
            }
        }

        public Vector4 GetRow(uint row)
        {
            switch (row)
            {
                case 0:
                    return new Vector4(m1, m5, m9, m13);
                case 1:
                    return new Vector4(m2, m6, m10, m14);
                case 2:
                    return new Vector4(m3, m7, m11, m15);
                case 3:
                    return new Vector4(m4, m8, m12, m16);
            }
            return new Vector4();
        }

        public Vector4 GetCol(uint col)
        {
            switch (col)
            {
                case 0:
                    return new Vector4(m1, m2, m3, m4);
                case 1:
                    return new Vector4(m5, m6, m7, m8);
                case 2:
                    return new Vector4(m9, m10, m11, m12);
                case 3:
                    return new Vector4(m13, m14, m15, m16);
            }
            return new Vector4();
        }

        public void Set(Matrix4 m)
        {
            m1 = m.m1;
            m2 = m.m2;
            m3 = m.m3;
            m4 = m.m4;

            m5 = m.m5;
            m6 = m.m6;
            m7 = m.m7;
            m8 = m.m8;

            m9 = m.m9;
            m10 = m.m10;
            m11 = m.m11;
            m12 = m.m12;

            m13 = m.m13;
            m14 = m.m14;
            m15 = m.m15;
            m16 = m.m16;
        }

        public void Set(Vector3 X, Vector3 Y, Vector3 Z)
        {
            m1 = X.x;
            m2 = X.y;
            m3 = X.z;

            m4 = Y.x;
            m5 = Y.y;
            m6 = Y.z;

            m7 = Z.x;
            m8 = Z.y;
            m9 = Z.z;
        }

        public void Set(float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9, float m10, float m11, float m12, float m13, float m14, float m15, float m16)
        {
            this.m1 = m1;
            this.m2 = m2;
            this.m3 = m3;
            this.m4 = m4;

            this.m5 = m5;
            this.m6 = m6;
            this.m7 = m7;
            this.m8 = m8;

            this.m9 = m9;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;

            this.m13 = m13;
            this.m14 = m14;
            this.m15 = m15;
            this.m16 = m16;
        }

        public void SetRotateX(double radians)
        {
            Set(1, 0, 0, 0,
                0, (float)Math.Cos(radians), (float)Math.Sin(radians), 0,
                0, -(float)Math.Sin(radians), (float)Math.Cos(radians), 0,
                0, 0, 0, 1);
        }

        public void SetRotateY(double radians)
        {
            Set((float)Math.Cos(radians), 0, -(float)Math.Sin(radians), 0,
                0, 1, 0, 0,
                (float)Math.Sin(radians), 0, (float)Math.Cos(radians), 0,
                0, 0, 0, 1);
        }

        public void SetRotateZ(double radians)
        {
            Set((float)Math.Cos(radians), (float)Math.Sin(radians), 0, 0,
                -(float)Math.Sin(radians), (float)Math.Cos(radians), 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        }

        public static Matrix4 operator *(Matrix4 lhs, Matrix4 rhs)
        {
            return new Matrix4(lhs.GetRow(0).Dot(rhs.GetCol(0)),
                                lhs.GetRow(1).Dot(rhs.GetCol(0)),
                                lhs.GetRow(2).Dot(rhs.GetCol(0)),
                                lhs.GetRow(3).Dot(rhs.GetCol(0)),

                                lhs.GetRow(0).Dot(rhs.GetCol(1)),
                                lhs.GetRow(1).Dot(rhs.GetCol(1)),
                                lhs.GetRow(2).Dot(rhs.GetCol(1)),
                                lhs.GetRow(3).Dot(rhs.GetCol(1)),

                                lhs.GetRow(0).Dot(rhs.GetCol(2)),
                                lhs.GetRow(1).Dot(rhs.GetCol(2)),
                                lhs.GetRow(2).Dot(rhs.GetCol(2)),
                                lhs.GetRow(3).Dot(rhs.GetCol(2)),

                                lhs.GetRow(0).Dot(rhs.GetCol(3)),
                                lhs.GetRow(1).Dot(rhs.GetCol(3)),
                                lhs.GetRow(2).Dot(rhs.GetCol(3)),
                                lhs.GetRow(3).Dot(rhs.GetCol(3)));
        }

        public static Vector4 operator *(Matrix4 lhs, Vector4 rhs)
        {
            return new Vector4(lhs.GetRow(0).Dot(rhs),
                                lhs.GetRow(1).Dot(rhs),
                                lhs.GetRow(2).Dot(rhs),
                                lhs.GetRow(3).Dot(rhs));
        }
    }
}
